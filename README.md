#WebView for ReactNative
 * This document contains a tutorial on how to use the component of webView in reactNative
 * It also contains methods to exchange data from ReactNative to webView
 *
 * Author: Philip Wang
 * Date  : June 3rd, 2017 

###What is webView:

A “webview” is a browser bundled inside of a mobile application producing what is called a hybrid app. 
Using a webview allows mobile apps to be built using Web technologies (HTML, JavaScript, CSS, etc.) but still package it as a native app and put it in the app store.
(https://www.stevesouders.com/blog/2014/10/09/do-u-webview/)

###Getting Started
1. Install reactNative by following the tutorial on the following link:
https://facebook.github.io/react-native/

2. Import the webView component in reactNative by adding the following lines:
import { WebView } from 'react-native';

3. Now, you can use the "<WevView / >" tag to utilize it, with props, of course.

Notes:

For all the props, please click the link below. Some of the important props will be explained in the next part as well.

https://facebook.github.io/react-native/docs/webview.html

For the communiation between the webView and the reactNative, the current
solution is to use the WebView props directly, instead of using a thrid part
component called webViewBridge, which, I believe is not competiable to the
current version of reactNative iOS, since more than 5 people have reported on
the gitHub saying they are encountering the same problem. The origial webView
tage in the latest version actually contains all the tool we normally use, and
they were relatively easy to utilize.

###Basic webView Code(From react-native website)
```javascript
    import { WebView } from 'react-native';
    class MyWeb extends Component {
        render() {
            return (
                <WebView
                    source={{uri: 'https://github.com/facebook/react-native'}}
                    style={{marginTop: 20}}
                />
            );
        }
    }
```

## WebView Bridge Section
####Preface:
By using the functions in reactNative WebView, we could build a bridge that allows the users to communiate from webView to reactNative, vise versa through strings. 

For now, I'm have a function in the web, and another function in react native, that will use the switch statment to go to the corresponding function. 

####Send Message from WebView to ReactNative
#####In the reactNative part, the user needs to
1. Import from WebView from 'react-native'
2. Use ```<WebView onMessage={this.onMessage} ref={( webView ) => this.webView = webView} \>```, where onMessage is a function in the same class
3. Set up an onMessage function that
```javascript
    onMessage(event) {
	    // Blablablablbla
	    event.nativeEvent.data // Access the string passed in by this 
	    // lead to different functions by using a switch statement
	    // for example
	    switch (event.nativeEvent.data) {
	        case "case1":
	            break;
	        default:
	            break;
	    }
    }
```
4. Have a constructor
```javascript
    constructor(props) {
	    super(props);
	    this.webView= null;
    }
```
#####In the webView part, the user needs to
1. Have a function that send out the string:
```javascript
    function postMes(message) {
	    window.postMessage(message, '*');
    };
```
####Send Message from ReactNative to WebView
#####In the reactNative part, the user needs to
1. Set up a button that has the props 
```javascript
    onPress={() => this.sendPostMessage(message)} // Where message is the string you want to send
```
2. Create a function called sendPostMessage that 
```javascript
    sendPostMessage(message) {
	    this.webView.postMessage(message)
    }
```

#####In the webView part, the user needs to
1. Create a function 
```javascript
    document.addEventListener("message", function(event) {
	    event.data // this is the string that was passed in from reactNative
    })
```

###End Notes
Basically, for now, if there is a function in webView, it could be called from reactNative, and vise versa, by using the webview
bridge explained above. For example, if we need to do a DOM manipulation, we could just have a function called
```javascript
	function addMeme() {
		$('#image').append("<img src='./corgiMeme.jpg' style='width: 80%'>");
	}
```
Whenever the we want to call this function from reactNative, we could send a string, therefore calling this function in
the webView.





